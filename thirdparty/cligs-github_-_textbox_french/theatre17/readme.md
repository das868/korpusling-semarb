Théâtre classique: 100 French plays 1640-1670
=============================================

## Contents

This collection is a subset of the _Théâtre classique_ collection published by Paul Fièvre at <http://www.theatre-classique.fr>. This is a curated sample collection containing 100 French plays from the classical age in several standard formats. 

* The plays have premiered between 1640 and 1670.
* The collection only contains comedies, tragedies and tragi-comedies
* There is a total number of 100 texts with 1.624 million words in total (or 16.240 words in average each)
* To make comparisons easier, only plays written in verse are included, excluding plays written partially or entirely in prose.
* Some basic metadata about the plays is available as a CSV file ("metadata.csv")
* The collection is meant for studying the differences between dramatic subgenres. Because the proportions of plays changes with each decade, the collection is not suited for an analysis of chronological development. A balanced subset would need to be created for such a purpose.

|genres/decades| 1640s texts  | 1650s texts | 1660s texts | total texts  |
|--------------|--------------|-------------|-------------|--------------|
|comédie       |         12   |        14   |         6   |         32   |
|tragédie      |         20   |         9   |        17   |         46   |
|tragicomédie  |          9   |         8   |         5   |         22   |
|total         |         41   |        31   |        28   |        100   |

* See <https://github.com/cligs/theatreclassique> for a larger number of plays derived from the <em>Théâtre classique</em> collection and transformed to TEI P5.


The TEI schema for the basic and the linguistically annotated TEI files corresponds to the general CLiGS schema which is available in the CLiGS [reference repository](https://github.com/cligs/reference).

The metadata keywords used in the text classification section of the TEI header are controlled by an external TEI keywords file and a schematron file which are stored in the [keywords](keywords) folder.

## Formats 

* Note that all plays have filenames following the pattern "author_short-title_id". 
* All plays are available as XML-TEI P5, which is the master format (folder "tei")
* Plays are also available as plain text versions, containing speaker text only (folder "txt")
* Plays are also available in a tagged version (part of speech and lemmata) based on the the PRESTO model for TreeTagger (folder "annotated")
* Finally, the plays are available in the "zwischenformat", an abstraction of the full text containing data relevant to network analysis of plays (folder "zwischenformat")

## License and Citation

All texts are in the public domain. The original TEI P4 markup and the metadata have been added by Paul Fièvre and his collaborators. The transformation to TEI P5 has been made by Ulrike Henny. The zwischenformat data has been extracted by Christof Schöch, inspired by work of the DLINA group. 

Please provide a reference if you use this research data in your teaching or research. The following is a citation suggestion: <em>Collection de pièces the théâtre français du dix-septième siècle</em>, edited by Christof Schöch, based on work by Paul Fièvre. Würzburg: CLiGS, 2017. URL: <https://github.com/cligs/textbox/master/french/theatre17/>. 



