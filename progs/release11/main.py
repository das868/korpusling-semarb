# -*- coding: utf-8 -*-

# to be invoked like this:
#   py main.py -s|-d 'full\path\to\filename.txt'|'full\path\to\files'
# output pngs will be exported into `imgs` subfolder TODO!

# -------------------------------------------------------------

# memo to myself:
# sys.argv[0]  = own filename
# sys.argv[1:] = custom cmd line args
# sys.argv[1]  = flags
# sys.argv[2]  = path to operate on


import own_lib as lib
import sys, time


def main():
    try:
        t0 = time.time()
        flag = sys.argv[1]
        if flag == '-s': # single txt file
            path_to_txt = sys.argv[2]
            #lib.custom_log('D', '', 'path_to_txt: ', path_to_txt) # DEBUG
            lib.handle_txt_file(path_to_txt)
        elif flag == '-d': # directory of txt files
            path_to_dir = sys.argv[2]
            paths_list = lib.get_file_list(path_to_dir)
            for txt_path in paths_list:
                lib.handle_txt_file(txt_path)
        else: # display help
            lib.display_help()
    except Exception as e:
        lib.custom_log('E', 'main', 'didn´t receive _one_ flag and path each in sys.argv, aborting...\n\tsys.argv was:\n\t\t', sys.argv)
        lib.custom_log('E', 'main', 'Exception thrown:\n\t\t', str(e))
    finally:
        t1 = time.time()
        print('\n\n[>Σ<]\ttotal runtime:\t', t1-t0, "\n", sep=None)
        return


if __name__ == '__main__':
    main()