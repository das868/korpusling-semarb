@echo off
echo "Cleaning up junk from testing for a fresh start..."

rm corr_coeffs.csv

rm testdata1.csv
rm testdata1-raw.png
rm testdata1-decluttered.png
rm testdata1-wordlist.txt

rm testdata2.csv
rm testdata2-raw.png
rm testdata2-decluttered.png
rm testdata2-wordlist.txt

rm testdata3.csv
rm testdata3-raw.png
rm testdata3-decluttered.png
rm testdata3-wordlist.txt

rm testdata4000.csv
rm testdata4000-raw.png
rm testdata4000-decluttered.png
rm testdata4000-wordlist.txt

echo "Cleanup successful."