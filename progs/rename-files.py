# -*- coding: utf-8 -*-


# a small utility program to rename
#   all roman/theatre filenames to match [a-z]{2}[0-9]{4}\.txt

# to be placed in desired input/output folder and executed there


import os


def main():
    dir = os.getcwd()
    for file in os.listdir(dir):
        if file.endswith('.txt'):
            # path_full = os.path.join(dir, file)
            f1 = open(file, "r", encoding='utf-8')
            content = f1.read()
            f1.close()

            # spl = file.split("_") # for theatre17
            # spl = file.split("-") # for roman19
            
            f2 = open(spl[-1], "w", encoding='utf-8')
            f2.write(content)
            f2.close()
            
            print('Wrote: ', spl[-1], ' to replace ', file)


if __name__ == '__main__':
    main()
    print('\nWorked.\n\n')
