# List of used Literature w/source

| Quality | Title as Resource-Link | Accessed |
| ---: | :--- | :--- |
| (-) | [Zipf's Law Everywhere](https://www.researchgate.net/profile/Wentian-Li/publication/253290454_Zipf's_Law_Everywhere/links/5cf59ef9299bf1fb185617ff/Zipfs-Law-Everywhere.pdf) | 29.07.2021, 09:10 |
| (--) | [Theory of Zipf's Law and Beyond](https://books.google.de/books?hl=de&lr=&id=orSSCf69ZhMC&oi=fnd&pg=PP5&dq=zipf%27s+law&ots=hcW1m6q46_&sig=aTfeH5RbSN6BSnx8VQpTnHtJRFM#v=onepage&q=zipf's%20law&f=false) | 29.07.2021, 09:13 |
| (++) | [A Relationship Between Lotka’s Law, Bradford’s Law, and Zipf’s Law](https://asistdl.onlinelibrary.wiley.com/doi/epdf/10.1002/%28SICI%291097-4571%28198609%2937%3A5%3C307%3A%3AAID-ASI5%3E3.0.CO%3B2-8?saml_referrer) | 29.07.2021, 09:16 (LMU Institutional Access) |
| (+) | [Applications and Explanations of Zipf's Law](https://aclanthology.org/W98-1218.pdf) | 29.07.2021, 09:24 |
| (++) | [Textsortenlinguistik (Christina Gansel) -> Kap. 1 Grundsatzdef.s!](https://elibrary.utb.de/doi/book/10.36198/9783838534596) | 11.08.2021, 15:35 (LMU Institutional Access) |

