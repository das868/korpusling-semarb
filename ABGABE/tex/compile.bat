@echo off
call cleanup.bat

pdflatex.exe -interaction=nonstopmode main.tex
pdflatex.exe -interaction=nonstopmode main.tex

bibtex.exe main

pdflatex.exe -interaction=nonstopmode main.tex
pdflatex.exe -interaction=nonstopmode main.tex

call cleanup.bat