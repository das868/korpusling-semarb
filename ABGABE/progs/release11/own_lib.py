# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import sys, os
from datetime import datetime


# ------------------ copied in from utils.py -------------------

# based off of:
#   Papula, Lothar: Mathematische Formelsammlung für Ingenieure
#     und Naturwissenschaftler, 10. Aufl. 2009, Vieweg+Teubner
#     Wiesbaden, S. 307-309.


# --------------------------------------------------------------
# shorts/utils:


log10 = lambda x: np.log(x) / np.log(10)


def custom_log(success_debug_error='S/D/E', error_location='', message='stub msg', rest=''):
    if success_debug_error == 'S':
        print('\n[***] SUCCESS:\n\t', message, rest, sep=None)
    elif success_debug_error == 'D':
        print('\n[ ? ] DEBUG:\n\t', message, rest, sep=None)
    elif success_debug_error == 'E':
        if not error_location == '':
            print('\n[ ! ] ERROR in ', error_location, ':\n\t', message, rest, sep=None)
        else:
            print('\n[!] GENERAL ERROR SOMEWHERE; message: ', message, rest, sep=None)
    else:
        print('[[[!!!]]] c h o o s e   a n   o p t i o n [[[!!!]]]')


# --------------------------------------------------------------
# statistical stuff:


def mean(liste: list()) -> float:
    return sum(liste)/len(liste)


def standard_dev_single(liste: list()):
    '''returns tuple (result, vi_sum)'''
    n = len(liste)
    if n <= 2:
        custom_log('E', 'standard_dev_single', 'len of input < 2, non usable')
        return 0
    else:
        x_bar = mean(liste)
        vi_list = [(el - x_bar) for el in liste]
        res = False
        s = sum(vi_list)
        if s == 0:
            res = True
        # custom_log('D', '', 'standard_dev_single: 0 ~?~ sum(vi_list) =', s) # sum of errors must vanish -> mean!
        nominator = sum([(el - x_bar)**2 for el in liste])
        denominator = n - 1
        return (np.sqrt(nominator / denominator), s)


def standard_dev_mean(liste: list()):
    '''returns tuple (result, vi_sum of underlying standard_dev_single)'''
    n = len(liste)
    if n <= 2:
        custom_log('E', 'standard_dev_single', 'len of input < 2, non usable')
        return 0
    else:
        res = standard_dev_single(liste)
        sds = res[0] # only care about result, not vi_sum
        vis = res[1] # keep vi_sum for debug outputting to file
        return (sds / np.sqrt(n), vis)


def delta_helper(xs: list()) -> float:
    term1 = len(xs) * sum(list(map(lambda x: x**2, xs)))
    term2 = sum(xs)**2
    return term1 - term2


def get_regr_line_params(xs: list(), ys: list()):   # -> (float, float)
    # check if xs and ys are same length = are points
    if len(xs) != len(ys):
        custom_log('E', 'get_regr_line_params', 'len`s not identical')
        return (0,0)

    # list values are usable
    # generate params a & b in y = ax + b
    n = len(xs)
    paired = list(zip(xs, ys))
    # print("paired: ", paired) # DEBUG
    delt = delta_helper(xs)

    # for a
    term_a1 = n * sum(list(map(lambda p: p[0]*p[1], paired)))
    term_a2 = sum(xs) * sum(ys)
    a = (term_a1 - term_a2) / delt

    # for b
    term_b1 = sum(list(map(lambda x: x**2, xs))) * sum(ys)
    term_b2 = sum(xs) * sum(list(map(lambda p: p[0]*p[1], paired)))
    b = (term_b1 - term_b2) / delt

    return (a,b) # y = ax + b


def corr_coeff(xs: list, ys: list) -> float:
    # check if xs and ys are same length = are points
    if len(xs) != len(ys):
        custom_log('E', 'corr_coeff', 'len`s not identical')
        return 2 # illegal value <= Error!

    # list values are usable
    n = len(xs)
    x_bar = mean(xs)
    y_bar = mean(ys)
    paired = list(zip(xs, ys))

    term_nom1 = sum(list(map(lambda p: p[0]*p[1], paired)))
    term_nom2 = n * x_bar * y_bar

    term_den_l1 = sum(list(map(lambda x: x**2, xs)))
    term_den_l2 = n * x_bar**2

    term_den_r1 = sum(list(map(lambda y: y**2, ys)))
    term_den_r2 = n * y_bar**2

    nominator = term_nom1 - term_nom2
    den_l = term_den_l1 - term_den_l2
    den_r = term_den_r1 - term_den_r2
    denominator = np.sqrt(den_l * den_r)

    return nominator / denominator


def test_papula_309f():
    xs = [0,2,3,5,8]
    ys = [0.6, 3.9, 5.8, 9.7, 14.6]
    cc = corr_coeff(xs, ys)
    rp = get_regr_line_params(xs, ys)
    print("\n[?] TESTING! (example Papula 309f.)\n", 
          "\n\txs: ", xs, 
          "\n\tys: ", ys,
          "\n\n\tcorr_coeff: ", cc, 
          "\n\tregr_params: ", rp,
          "\n\n\tTests successful?: ", (cc == 0.9993844353389921 and rp == (1.77258064516129, 0.5387096774193563)))


# DEBUG!
#test_papula_309f()


# --------------------------------------------------------------
# own custom stuff:


def create_goodbadlist(data, exs):   # -> (list, list)
    entries = list(map(lambda a: a.split(","), data))
    entries = list(map(lambda a: [a[0], a[1], a[2][:-1]], entries))
    #custom_log('D', '', 'entries:\t', entries) # DEBUG!

    goodlist = list()
    badlist = list()

    for ent in entries:
        for ex in exs:
            if ent[2] == ex:
                badlist.append(ent)
                break
            elif ex == exs[-1]:
                goodlist.append(ent)


    ## print('exc_list(', len(exception_list), '): ', exception_list, '\n\ngoodlist(', len(goodlist), '): ', goodlist, '\n\nbadlist(', len(badlist), '): ', badlist)
    #print('exs(', len(exs), '): ', exs, '\n\ngoodlist(', len(goodlist), '): ', goodlist, '\n\nbadlist(', len(badlist), '): ', badlist) # DEBUG

    custom_log('S', '', 'good/bad\\list generation successful.')
    return (goodlist, badlist)


def test_create_goodbadlist():
    path1 = 'C:/users/wemod/desktop/doof/release5/exlist.txt'
    path2 = 'C:/users/wemod/desktop/doof/release5/testdata1.csv'

    file1 = open(path1, "r", encoding='utf-8')
    exlist = file1.read().replace("\n", " ").replace("  ", " ").split(" ")[:-1]
    exs = list(set(exlist))
    file1.close()

    file2 = open(path2, "r", encoding='utf-8')
    testdata = file2.readlines()
    file2.close()

    res = create_goodbadlist(testdata, exs)
    #print("goodlist: ", res[0], "\nlen: ", len(res[0])) # DEBUG


# DEBUG!
#test_create_goodbadlist()


def create_goodbadlist_hapax(data, exs, hapax_level_limit=10):   # -> (list, list)
    entries = list(map(lambda a: a.split(","), data))
    entries = list(map(lambda a: [a[0], a[1], a[2][:-1]], entries))
    #custom_log('D', '', 'entries:\t', entries) # DEBUG!

    goodlist = list()
    badlist = list()

    for ent in entries:
        if ent[1] in map(str, range(1, hapax_level_limit)):
            badlist.append(ent)
            continue
        for ex in exs:
            if ent[2] == ex:
                badlist.append(ent)
                break
            elif ex == exs[-1]:
                goodlist.append(ent)

    #goodlist = goodlist[32:]
    #custom_log('D', '', 'goodlist:\n\t\t', goodlist)


    ## print('exc_list(', len(exception_list), '): ', exception_list, '\n\ngoodlist(', len(goodlist), '): ', goodlist, '\n\nbadlist(', len(badlist), '): ', badlist)
    #print('exs(', len(exs), '): ', exs, '\n\ngoodlist(', len(goodlist), '): ', goodlist, '\n\nbadlist(', len(badlist), '): ', badlist) # DEBUG

    custom_log('S', '', 'good/bad\\list generation successful.')
    return (goodlist, badlist)


def ranking_close_gaps(goodlist):
    index = 0
    while index < len(goodlist):
        if (index == 0) and (goodlist[0][0] != '1'):
            correct = int(goodlist[0][0]) - 1
            goodlist[0][0] = '1'
            for i in range(1, len(goodlist)):
                goodlist[i][0] = str(int(goodlist[i][0]) - correct)
        elif goodlist[index][0] != str(index+1):
            correct = int(goodlist[index][0]) - index - 1
            goodlist[index][0] = str(index + 1)
            for i in range(index + 1, len(goodlist)):
                goodlist[i][0] = str(int(goodlist[i][0]) - correct)

        index += 1
    return goodlist


def test_ranking_close_gaps():
    path1 = 'C:/users/wemod/desktop/doof/release5/exlist.txt'
    path2 = 'C:/users/wemod/desktop/doof/release5/testdata1.csv'

    file1 = open(path1, "r", encoding='utf-8')
    exlist = file1.read().replace("\n", " ").replace("  ", " ").split(" ")[:-1]
    exs = list(set(exlist))
    file1.close()

    file2 = open(path2, "r", encoding='utf-8')
    testdata = file2.readlines()
    file2.close()

    res = create_goodbadlist(testdata, exs)
    ans = ranking_close_gaps(res[0])
    print("re-closed ranking: ", ans, 
          "\nlen goodlist: ", len(res[0]), 
          "\nlen re-closed: ", len(ans),
          "\n[?] TEST successful?: ", len(res[0]) == len(ans))


# DEBUG!
#test_ranking_close_gaps()


def check_ranks_continuous(goodlist) -> bool:
    res = False
    for i in range(0,len(goodlist)):
        if goodlist[i][0] != str(i+1):
            errstr = 'new ranks not continuous; failure: @' + str(i) + ' :  ' + str(goodlist[i])
            custom_log('E', 'check_ranks_continuous', errstr)
            break
        elif i == len(goodlist) - 1:
            custom_log('S', '', 'new ranks are continuous')
            res = True
    return res


def test_check_ranks_continuous():
    path1 = 'C:/users/wemod/desktop/doof/release5/exlist.txt'
    path2 = 'C:/users/wemod/desktop/doof/release5/testdata1.csv'

    file1 = open(path1, "r", encoding='utf-8')
    exlist = file1.read().replace("\n", " ").replace("  ", " ").split(" ")[:-1]
    exs = list(set(exlist))
    file1.close()

    file2 = open(path2, "r", encoding='utf-8')
    testdata = file2.readlines()
    file2.close()

    res = create_goodbadlist(testdata, exs)
    ans = ranking_close_gaps(res[0])
    print("\n[ ? ] DEBUG:\n\re-closed ranking: ", ans, 
          "\n\t\tlen goodlist: ", len(res[0]), 
          "\n\t\tlen re-closed: ", len(ans),
          "\n\t\tcheck ranks cont.?: ", check_ranks_continuous(ans),
          "\n[ ? ] DEBUG:\n\tTEST successful?: ", len(res[0]) == len(ans))


# DEBUG!
#test_check_ranks_continuous()


def create_csv_from_txt(path_to_file: str) -> str:
    outpname = str()

    try:
        the_file = open(path_to_file, "r", encoding='utf-8')
        content1 = the_file.readlines()
        content2 = content1[3:]
        content3 = list(map(lambda a: a.replace("\t", ","), content2))
        content4 = list(map(lambda a: a.replace(",\n", "\n"), content3))
        outpname = path_to_file[:-4] + '.csv'
        try:
            out_file = open(outpname, "w", encoding='utf-8')
            out_file.writelines(content4)
            custom_log('S', '', 'File created:\t', outpname)
        except Exception as e1:
            custom_log('E', 'create_csv_from_txt (1)', str(e1))
        finally:
            out_file.close()
    except Exception as e2:
        custom_log('E', 'create_csv_from_txt (2)', str(e2))
    finally:
        the_file.close()
    
    return outpname


def test_create_csv_from_txt(path):
    custom_log('D', '', 'TESTING generated file:\n\t\t', create_csv_from_txt(path))


# DEBUG!
#test_create_csv_from_txt("c:/users/wemod/desktop/doof/release5/testdata1.txt")


def custom_plot(ranks_list, freqs_list, mark: str, path_to_csv, show=True):
    param1, param2 = get_regr_line_params(ranks_list, freqs_list)
    custom_log('D', '', 'param1/2:\t', (str(param1) + ' / ' + str(param2))) # DEBUG!
    cc = corr_coeff(ranks_list, freqs_list)
    custom_log('D', '', 'corr_coeff:\t', cc) # DEBUG!
    make_line = lambda x: param1 * x + param2
    regr_line = make_line(ranks_list)

    plt.plot(ranks_list, freqs_list, label='plot')
    plt.plot(ranks_list, regr_line, label='regr')
    plt.xlabel(r'$\log_{10}(rank)$')
    plt.ylabel(r'$\log_{10}(frequency)$')
    plt.legend()
    percent = abs(cc) * 100
    # percent_round = round(abs(cc) * 100, 5)
    try:
        # percent_split = str(abs(cc*100)).split(".")
        # percent_export = percent_split[0] + "." + percent_split[1][:5] + " " + percent_split[1][5:]#  + "\n"
        cc_save_path = path_to_csv.split("/")

        if len(cc_save_path) == 1:
            # no slashes but backslashes used in sys.argv[1]
            cc_save_path = path_to_csv.split("\\")
        
        start_path = path_to_csv[:-len(cc_save_path[-1])] # get rid of csv file's name
        end_path = path_to_csv[-len(cc_save_path[-1]):]
        filename_used = end_path[:-4]

        save_path = start_path + 'corr_coeffs.csv'
        g = open(save_path, "a", encoding='utf-8')
        g.write((filename_used) + '-' + mark)
        g.write(",")
        # g.write(percent_export)
        g.write(str(percent))
        g.write(",")
        g.write(str(param1))
        g.write(",")
        g.write(str(param2))
        g.write("\n")

        # debugstr = 'path_to_csv:\t ' + path_to_csv + '\n\t save_path:\t ' + save_path + '\n\t p_to_csv[0]:\t ' + start_path + '\n\t p_to_csv[ω]:\t ' + end_path + '\n\t in_filename:\t ' + filename_used + '\n\t perc_exprt:\t ' + percent_export # DEBUG!
        debugstr = 'path_to_csv:\t ' + path_to_csv + '\n\t save_path:\t ' + save_path + '\n\t p_to_csv[0]:\t ' + start_path + '\n\t p_to_csv[ω]:\t ' + end_path + '\n\t in_filename:\t ' + filename_used + '\n\t percent:\t ' + str(percent) # DEBUG!
        custom_log('D', '', debugstr) # DEBUG!

        custom_log('S', '', 'File appended:\t', save_path)
    except Exception as e:
        custom_log('E', 'custom_plot (percent_export)', str(e))
    finally:
        g.close()
    
    titlestr = 'Regression on ' + mark + ' rank/freq data\n' + r'$r = $' + str(percent) + r'%'
    plt.title(titlestr)
    fig = plt.gcf()
    if show:
        plt.show()
    figpath = path_to_csv[:-4] + '-' + mark + '.png'
    fig.savefig(figpath, dpi=600)
    custom_log('S', '', 'File created:\t', figpath)

    # re-uninitialize all variables for next fresh turn
    param1 = param2 = regr_line = None # probably unnecessary...
    fig.clear() # crucial


def test_custom_plot(mark, path_to_csv):
    data = np.genfromtxt(path_to_csv, delimiter=',', names=['rank', 'freq', 'word'])
    ranks_list = log10(data['rank'])
    freqs_list = log10(data['freq'])
    custom_plot(ranks_list, freqs_list, mark, path_to_csv)


# DEBUG!
#test_custom_plot('raw', create_csv_from_txt('c:/users/wemod/desktop/doof/release5/testdata1.txt'))


def parse_exlist(path='exlist.txt') -> list:
    exs = list()
    try:
        file = open(path, "r", encoding='utf-8')
        exs2 = file.read().replace("\n", " ").replace("  ", " ").split(" ")[:-1]
        exs = list(set(exs2))
        #custom_log('D', '', 'parse_exlist:\t', exs2) # DEBUG!
    except Exception as e:
        custom_log('E', 'parse_exlist', str(e))
    finally:
        file.close()
        return exs


def test_parse_exlist():
    res = parse_exlist('c:/users/wemod/desktop/doof/release5/exlist.txt')


# DEBUG!
#test_parse_exlist()


def create_sep_pngs_from_csv(path_to_csv: str, exs: list, show=False):
    # section1 : raw data
    data_raw = list()
    try:
        data_raw = np.genfromtxt(path_to_csv, delimiter=',', names=['rank', 'freq', 'word'])
    except Exception as e:
        custom_log('E', 'create_sep_pngs_from_csv (1): Exception thrown:\n\t\t', 
                    str(e) + '\n[!] CRITICAL ERROR: PROGRAM ABORT DUE TO FAULTY DATA!\n')
        return
    
    ranks_list = log10(data_raw['rank'])
    freqs_list = log10(data_raw['freq'])

    custom_plot(ranks_list, freqs_list, 'raw', path_to_csv, show)

    # section2: decluttered data
    try:
        file = open(path_to_csv, "r", encoding='utf-8')
        data_raw = file.readlines()
    except Exception as e:
        custom_log('E', 'create_sep_pngs_from_csv (2): Exception thrown:\n\t\t', 
                    str(e) + '\n[!] CRITICAL ERROR: PROGRAM ABORT DUE TO FAULTY DATA!\n')
        return
    finally:
        file.close()
    
    goodlist, badlist = create_goodbadlist_hapax(data_raw, exs, 10)
    goodlist2 = ranking_close_gaps(goodlist)
    custom_log('D', '', 'check ranks re-continued?:\t', check_ranks_continuous(goodlist2))

    #ranks_list2 = np.array(list(map(lambda x: log10(float(x)), [el[0] for el in goodlist2])))
    #freqs_list2 = np.array(list(map(lambda x: log10(float(x)), [el[1] for el in goodlist2])))

    ranks_list2 = np.array([log10(float(x)) for x in [el[0] for el in goodlist2]])
    freqs_list2 = np.array([log10(float(x)) for x in [el[1] for el in goodlist2]])

    custom_plot(ranks_list2, freqs_list2, 'decluttered', path_to_csv, show)

    # create wordlist txt file after decluttering
    try:
        path = path_to_csv[:-4] + '-wordlist.txt'
        file = open(path, "w", encoding='utf-8')
        for el in goodlist2:
            file.write(el[2]) # words column
            file.write("\n")
        custom_log('S', '', 'File created:\t', path)
    except Exception as e:
        custom_log('E', 'wordlist generation in create_sep_pngs_from_csv: Exception thrown:\n\t\t', 
                    str(e) + '\n[!] \t\tUNABLE TO WRITE WORDLIST, SKIPPING...\n')
    finally:
        file.close()
            

def test_create_sep_pngs_from_csv(show):
    create_sep_pngs_from_csv(
        create_csv_from_txt('c:/users/wemod/desktop/doof/release5/testdata1.txt'), 
        parse_exlist('c:/users/wemod/desktop/doof/release5/exlist.txt'),
        show)


# DEBUG!
#test_create_sep_pngs_from_csv(True)


def display_help():
    print('? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?',
                '? ? ? ? ? ? ? ? ? ?   H E L P   ? ? ? ? ? ? ? ? ? ?',
                '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?',
                '',
                '\t -s:   full or relative path to _s_ingle txt input file',
                '\t -d:   full or relative path to _d_irectory of txt input files',
                '\t -h:   display this help message (for any invalid arguments also)\n',
                sep="\n")


def handle_txt_file(path_to_txt):
    print('\n\n--------------------------------------------------------------------------------\n\n')
    path_to_csv = create_csv_from_txt(path_to_txt)
    exlist = parse_exlist()
    create_sep_pngs_from_csv(path_to_csv, exlist, False)


def get_file_list(path_to_txt_folder):
    # / and \ may be mixed, no problem!
    txt_file_list = os.listdir(path_to_txt_folder)
    goodlist = [item for item in txt_file_list if (item[-4:] == '.txt' and item != 'exlist.txt')]
    paths_list = [(path_to_txt_folder + '/' + file) for file in goodlist]
    return paths_list


def post_process_csv(input_path):
    '''returns tuple (r_list, a_list, b_list)'''
    r_list = a_list = b_list = list()
    try:
        f = open(input_path, "r", encoding='utf-8')
        csv_lines = f.readlines()
        csv_data = list() # list of lists of filename, r, a, b (see below)
        # csv_data = dict()
        for line in csv_lines:
            if line[-1] == "\n":
                line = line[:-1]
            temp = line.split(",")
            filename = temp[0]
            temp[1] = float(temp[1]) # r corr.coeff. of plot vs. regr
            temp[2] = float(temp[2]) # a in regr = a*x + b
            temp[3] = float(temp[3]) # b in regr = a*x + b
            csv_data.append(temp)
            # csv_data[filename] = (temp[1], temp[2], temp[3])

        # desiderate: std_dev of r, a, b to get indication for type of text
        # print(csv_data) # DEBUG!
        r_list = [elem[1] for elem in csv_data]
        a_list = [elem[2] for elem in csv_data]
        b_list = [elem[3] for elem in csv_data]
        # r_list = [for filename in csv_data]
    except Exception as e:
        custom_log('E', 'post_process_csv', 'Exception thrown:\n\t\t', str(e))
    finally:
        f.close()
        return (r_list, a_list, b_list)


def extract_errorbar_data(input_path):
    '''returns tuple (r_data, a_data, b_data) where x_data = [x, std_dev_sg, std_dev_mn]'''
    # rab = std_dev_sg_rab = std_dev_mn_rab = list()
    r_data = a_data = b_data = list()
    try:
        f = open(input_path, "r", encoding='utf-8')
        contents = f.read()
        contents_lines = contents.split("\n")
        contents_split = [x.split(",") for x in contents_lines if x != '']
        contents_split_floats = [list(map(float, x)) for x in contents_split]
        
        r_data = contents_split_floats[0]
        a_data = contents_split_floats[1]
        b_data = contents_split_floats[2]

        # rab = [item[0] for item in contents_split3]
        # std_dev_sg_rab = [item[1] for item in contents_split3]
        # std_dev_mn_rab = [item[2] for item in contents_split3]        
    except Exception as e:
        custom_log('E', 'errorbar_from_csv', 'Exception thrown:\n\t\t', str(e))
    finally:
        f.close()
        return (r_data, a_data, b_data)
        # return (rab, std_dev_sg_rab, std_dev_mn_rab)


def custom_plot_err2(xs: list, ys: list, std_dev_mn_ys: list, t: float):
    # errs_uppr = list()
    # errs_down = list()
    # for i in range(0,len(y)):
    #     errs_uppr.append(ys[i] + t * std_dev_mn_ys[i])
    #     errs_down.append(ys[i] - t * std_dev_mn_ys[i])
    errs_uppr = list(map(lambda k:    k * t, std_dev_mn_ys))
    errs_down = list(map(lambda k: -(k * t), std_dev_mn_ys))
    errs = [errs_down, errs_uppr]
    custom_log('D', '', 'errors (uppr/down): ', str((errs_uppr, errs_down)))
    print("ys: ", ys)
    print("\nstd_dev_mn(y): ", std_dev_mn_ys)
    plt.errorbar(xs, ys, yerr=errs, fmt="ro", ecolor = "black")
    plt.show()
    fig = plt.gcf()
    fig.clear()


def custom_plot_err2(xs, ys, t):
    std_dev_mn_ys, goodness = standard_dev_mean(ys)
    # errs_uppr = list()
    # errs_down = list()
    # for i in range(0,len(ys)):
    #     errs_uppr.append(ys[i] + t * std_dev_mn_ys[i])
    #     errs_down.append(ys[i] - t * std_dev_mn_ys[i])
    errs_uppr = list(map(lambda k: k + t * std_dev_mn_ys, ys))
    errs_uppr[0] = 50
    errs_down = list(map(lambda k: k - t * std_dev_mn_ys, ys))
    # errs_uppr = list(map(lambda k: k + t * std_dev_mn_ys, ys))
    # errs_down = list(map(lambda k: k - t * std_dev_mn_ys, ys))
    errs = [errs_down, errs_uppr]
    custom_log('D', '', 'errors (uppr/down): ', str((errs_uppr, errs_down)))
    print("ys: ", ys)
    print("\nstd_dev_mn(ys): ", std_dev_mn_ys)
    plt.errorbar(xs, ys, yerr=errs, fmt="ro", ecolor = "black") # yerrs needs relative error, to be added to value!
    plt.show()
    fig = plt.gcf()
    fig.clear()