@echo off
echo "Generating csv´s and png´s from word_rankings..."
py main.py -d ../../data/word_rankings/nouvelles19
py main.py -d ../../data/word_rankings/roman19
py main.py -d ../../data/word_rankings/theatre17
echo "Done generating."

echo "Starting post_analyze..."
py post_analyze.py
echo "Done post_analyzing, results are in this folder."

echo "Everything worked."