# -*- coding: utf-8 -*-

# to be invoked like this:
#   py main.py -s|-d 'full\path\to\filename.txt'|'full\path\to\files'
# output pngs will be exported into `imgs` subfolder TODO!

# -------------------------------------------------------------

# memo to myself:
# sys.argv[0]  = own filename
# sys.argv[1:] = custom cmd line args
# sys.argv[1] = path to operate on
# sys.argv[2] = marker for output filename `results_rab_marker.{csv|txt}`


import own_lib as lib
import sys, time
import matplotlib.pyplot as plt
from scipy.stats import shapiro


def main():
    try:
        t0 = time.time()
        # input_path = sys.argv[1]
        input_paths = ['../../data/word_rankings/nouvelles19/corr_coeffs.csv',
                       '../../data/word_rankings/roman19/corr_coeffs.csv',
                       '../../data/word_rankings/theatre17/corr_coeffs.csv']
        marks = list()

        rmnl = list() # r mean list
        amnl = list() # a mean list
        bmnl = list() # b mean list
        
        rsdml = list() # r sdm list
        asdml = list() # a sdm list
        bsdml = list() # b sdm list

        shapiro_out = open('shapiro_results.csv', 'a', encoding='utf-8')

        for ip in input_paths:
            lib.custom_log('D', '', 'now treating: ', ip)
            mark = ip.split("/")[-2] # yields nouvelles19|roman19|theatre17
            marks.append(mark)
        
            #read csv data from some corr_coeffs.csv file & compute mean/std_dev`s
            r_list, a_list, b_list = lib.post_process_csv(ip)

            r_mn = lib.mean(r_list)
            r_sds = lib.standard_dev_single(r_list)
            r_sdm = lib.standard_dev_mean(r_list)
            lib.custom_log('S', '', 'mean(rs):       ', r_mn)
            lib.custom_log('S', '', 'std_dev_sg(rs): ', r_sds)
            lib.custom_log('S', '', 'std_dev_mn(rs): ', r_sdm)
            rmnl.append(r_mn)
            rsdml.append(r_sdm[0])
            name = mark + '_r'
            shapiro_out.write(name)
            shapiro_out.write(",")
            shapiro_out.write(str(shapiro(r_list)))
            shapiro_out.write("\n")
            # print("\n\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", shapiro(r_list), end="\n\n\n\n\n")

            a_mn = lib.mean(a_list)
            a_sds = lib.standard_dev_single(a_list)
            a_sdm = lib.standard_dev_mean(a_list)
            lib.custom_log('S', '', 'mean(as):       ', a_mn)
            lib.custom_log('S', '', 'std_dev_sg(as): ', a_sds)
            lib.custom_log('S', '', 'std_dev_mn(as): ', a_sdm)
            amnl.append(a_mn)
            asdml.append(a_sdm[0])
            name = mark + '_a'
            shapiro_out.write(name)
            shapiro_out.write(",")
            shapiro_out.write(str(shapiro(a_list)))
            shapiro_out.write("\n")
            # print("\n\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", shapiro(a_list), end="\n\n\n\n\n")

            b_mn = lib.mean(b_list)
            b_sds = lib.standard_dev_single(b_list)
            b_sdm = lib.standard_dev_mean(b_list)
            lib.custom_log('S', '', 'mean(bs):       ', b_mn)
            lib.custom_log('S', '', 'std_dev_sg(bs): ', b_sds)
            lib.custom_log('S', '', 'std_dev_mn(bs): ', b_sdm)
            bmnl.append(b_mn)
            bsdml.append(b_sdm[0])
            name = mark + '_b'
            shapiro_out.write(name)
            shapiro_out.write(",")
            shapiro_out.write(str(shapiro(b_list)))
            shapiro_out.write("\n")
            # print("\n\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", shapiro(b_list), end="\n\n\n\n\n")
            #continue
            # write to machine-readable csv file
            #   result: lines with format param,std_dev_sg,std_dev_mn{\n}
            try:
                outp_f1 = 'results_rab_' + mark + '.csv'
                g = open(outp_f1, "w", encoding='utf-8') # overwrite existing file bc easier to debug
                            
                # rs
                g.write(str(r_mn))
                g.write(",")
                g.write(str(r_sds[0]))
                g.write(",")
                g.write(str(r_sdm[0]))
                g.write("\n")

                # as
                g.write(str(a_mn))
                g.write(",")
                g.write(str(a_sds[0]))
                g.write(",")
                g.write(str(a_sdm[0]))
                g.write("\n")

                # bs
                g.write(str(b_mn))
                g.write(",")
                g.write(str(b_sds[0]))
                g.write(",")
                g.write(str(b_sdm[0]))
                g.write("\n")            
            except Exception as e3:
                lib.custom_log('E', 'main (3)', 'Exception thrown:\n\t\t', str(e3))
            finally:
                g.close()

            # write to human-readable txt file
            try:
                outp_f2 = 'results_rab_' + mark + '.txt'
                h = open(outp_f2, "w", encoding='utf-8') # overwrite existing file bc easier to debug
                h.write('---------\n|Results|\n---------\n\n')
                h.write('Format: std_dev_sg/mn(xs):\t(result, vi_sum=errorsum)\n\n')
                
                # rs
                h.write('mean(rs):      \t')
                h.write(str(r_mn))
                h.write("\n")

                h.write('std_dev_sg(rs):\t')
                h.write(str(r_sds))
                h.write("\n")

                h.write('std_dev_mn(rs):\t')
                h.write(str(r_sdm))
                h.write("\n\n")

                # as
                h.write('mean(as):      \t')
                h.write(str(a_mn))
                h.write("\n")

                h.write('std_dev_sg(as):\t')
                h.write(str(a_sds))
                h.write("\n")

                h.write('std_dev_mn(as):\t')
                h.write(str(a_sdm))
                h.write("\n\n")

                # bs
                h.write('mean(bs):      \t')
                h.write(str(b_mn))
                h.write("\n")

                h.write('std_dev_sg(bs):\t')
                h.write(str(b_sds))
                h.write("\n")

                h.write('std_dev_mn(bs):\t')
                h.write(str(b_sdm))
                h.write("\n\n")
            except Exception as e4:
                lib.custom_log('E', 'main (4)', 'Exception thrown:\n\t\t', str(e4))
            finally:
                h.close()

        # make plotting monolithic in this file!
        # lowr = list(map(lambda x: -x, rsdml))
        # uppr = rsdml
        # errs = [lowr, uppr]
        # 28 novels, 36 romans, 100 theatres
        # => t = 2.05,     2.05,     1.98    (@95% conf., Papula p. 302)
        r_errs = [rsdml[0] * 2.05, rsdml[1] * 2.05, rsdml[2] * 1.98]
        a_errs = [asdml[0] * 2.05, asdml[1] * 2.05, asdml[2] * 1.98]
        b_errs = [bsdml[0] * 2.05, bsdml[1] * 2.05, bsdml[2] * 1.98]
        

        # plot rs
        capt_x_r = [r'$r_{' + marks[0] + r'}$',
                    r'$r_{' + marks[1] + r'}$',
                    r'$r_{' + marks[2] + r'}$']
        (_, caps, _) = plt.errorbar(capt_x_r, rmnl, yerr=r_errs, fmt='o', capsize=500) # symmetric errors in y-axis!
        for cap in caps:
            cap.set_color('red')
        plt.title('Korrelationskoeffizienten nach Textsorte')
        # plt.show()
        fig = plt.gcf()
        figpath = 'plot_rs_all.png'
        fig.savefig(figpath, dpi=600)
        lib.custom_log('S', '', 'File created:\t', figpath)
        fig.clear()


        # plot as
        capt_x_a = [r'$a_{' + marks[0] + r'}$',
                  r'$a_{' + marks[1] + r'}$',
                  r'$a_{' + marks[2] + r'}$']
        (_, caps, _) = plt.errorbar(capt_x_a, amnl, yerr=a_errs, fmt='o', capsize=500) # symmetric errors in y-axis!
        for cap in caps:
            cap.set_color('red')
        plt.title(r'Regressionsparameter $a$ nach Textsorte')
        # plt.show()
        fig = plt.gcf()
        figpath = 'plot_as_all.png'
        fig.savefig(figpath, dpi=600)
        lib.custom_log('S', '', 'File created:\t', figpath)
        fig.clear()


        # plot bs
        capt_x_b = [r'$b_{' + marks[0] + r'}$',
                    r'$b_{' + marks[1] + r'}$',
                    r'$b_{' + marks[2] + r'}$']
        (_, caps, _) = plt.errorbar(capt_x_b, bmnl, yerr=b_errs, fmt='o', capsize=500)#, elinewidth=3) # symmetric errors in y-axis!
        for cap in caps:
            cap.set_color('red')
        plt.title(r'Regressionsparameter $b$ nach Textsorte')
        # plt.show()
        fig = plt.gcf()
        figpath = 'plot_bs_all.png'
        fig.savefig(figpath, dpi=600)
        lib.custom_log('S', '', 'File created:\t', figpath)
        fig.clear()
    except Exception as e:
        lib.custom_log('E', 'main', 'didn´t receive _one_ path in sys.argv, aborting...\n\tsys.argv was:\n\t\t', sys.argv)
        lib.custom_log('E', 'main', 'Exception thrown:\n\t\t', str(e))
    finally:
        shapiro_out.close()
        t1 = time.time()
        print('\n\n[>Σ<]\ttotal runtime:\t', t1-t0, "\n", sep=None)
        return


if __name__ == '__main__':
    main()