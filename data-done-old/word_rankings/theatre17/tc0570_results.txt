#Word Types: 2374
#Word Tokens: 18812
#Search Hits: 0
1	517	l	
2	489	et	
3	457	de	
4	369	le	
5	361	à	
6	331	il	
7	314	un	
8	308	que	
9	292	en	
10	291	qu	
11	282	d	
12	270	je	
13	260	vous	
14	259	la	
15	241	est	
16	212	pour	
17	191	ce	
18	172	ne	
19	167	mon	
20	166	me	
21	154	qui	
22	153	m	
23	152	si	
24	146	j	
25	139	n	
26	136	on	
27	134	s	
28	130	son	
29	122	mais	
30	121	par	
31	118	ma	
32	115	a	
33	106	dans	
34	101	lui	
35	99	sa	
36	94	plus	
37	92	du	
38	92	les	
39	90	tout	
40	77	moi	
41	74	coeur	
42	73	ai	
43	73	fait	
44	73	tu	
45	72	peut	
46	69	au	
47	68	se	
48	68	seigneur	
49	67	sans	
50	67	une	
51	63	c	
52	63	t	
53	62	votre	
54	60	pas	
55	60	voir	
56	59	crime	
57	57	eucherius	
58	57	trop	
59	57	être	
60	56	dont	
61	56	où	
62	55	fils	
63	55	ton	
64	51	rien	
65	50	mort	
66	49	quoi	
67	49	sur	
68	46	quand	
69	46	sang	
70	45	mes	
71	45	point	
72	43	amour	
73	43	des	
74	43	faire	
75	43	zénon	
76	42	elle	
77	41	te	
78	40	bien	
79	40	enfin	
80	40	non	
81	39	coupable	
82	39	seul	
83	39	y	
84	37	ta	
85	37	vertu	
86	36	pu	
87	36	trône	
88	34	gloire	
89	33	contre	
90	33	secret	
91	33	sort	
92	32	leur	
93	32	moins	
94	32	même	
95	32	nous	
96	31	orgueil	
97	31	toi	
98	31	vain	
99	31	vos	
100	30	ah	
101	30	empereur	
102	30	vois	
103	29	avec	
104	29	cette	
105	29	ses	
106	29	toujours	
107	28	faut	
108	28	ose	
109	28	puis	
110	27	comme	
111	27	doit	
112	27	lâche	
113	27	peine	
114	27	tous	
115	26	peu	
116	25	aime	
117	25	ardeur	
118	25	aux	
119	25	va	
120	24	cet	
121	24	ou	
122	23	ces	
123	23	dois	
124	23	espoir	
125	23	madame	
126	23	âme	
127	23	éclat	
128	22	croire	
129	22	grand	
130	22	là	
131	22	ordre	
132	22	rendre	
133	22	zèle	
134	21	eut	
135	21	fierté	
136	21	maître	
137	21	pouvoir	
138	21	quel	
139	21	voeux	
140	20	aimer	
141	20	assez	
142	20	attentat	
143	20	encore	
144	20	mieux	
145	20	quelque	
146	20	rend	
147	20	su	
148	19	cherche	
149	19	destin	
150	19	felix	
151	19	main	
152	19	rang	
153	19	suis	
154	19	traître	
155	19	veut	
156	18	aveu	
157	18	avoir	
158	18	foi	
159	18	horreur	
160	18	soeur	
161	17	autre	
162	17	ciel	
163	17	effet	
164	17	ils	
165	17	innocent	
166	17	jamais	
167	17	père	
168	17	stilicon	
169	17	vu	
170	16	beau	
171	16	connaître	
172	16	craindre	
173	16	devoir	
174	16	forfait	
175	16	indigne	
176	16	ingrat	
177	16	malheur	
178	16	punir	
179	16	tant	
180	16	veux	
181	15	ayant	
182	15	erreur	
183	15	innocence	
184	15	jours	
185	15	jusqu	
186	15	juste	
187	15	ont	
188	15	péril	
189	15	toute	
190	15	trouve	
191	15	voit	
192	15	ô	
193	14	as	
194	14	effort	
195	14	entreprise	
196	14	eux	
197	14	fureur	
198	14	naissance	
199	14	prix	
200	13	ait	
201	13	craint	
202	13	criminel	
203	13	deux	
204	13	laisse	
205	13	mépris	
206	13	princesse	
207	13	refus	
208	13	soins	
209	13	tendresse	
210	13	triste	
211	12	ainsi	
212	12	appui	
213	12	après	
214	12	coup	
215	12	crû	
216	12	flamme	
217	12	honte	
218	12	hymen	
219	12	lors	
220	12	mien	
221	12	perdre	
222	12	plaindre	
223	12	prendre	
224	12	surprise	
225	12	temps	
226	12	tes	
227	12	était	
228	12	état	
229	11	audace	
230	11	avait	
231	11	choix	
232	11	digne	
233	11	donc	
234	11	donne	
235	11	doux	
236	11	empire	
237	11	fais	
238	11	feu	
239	11	fut	
240	11	grâce	
241	11	hélas	
242	11	ici	
243	11	intérêt	
244	11	offre	
245	11	perte	
246	11	rage	
247	11	remords	
248	11	savoir	
249	11	secours	
250	11	sont	
251	11	sujet	
252	11	vie	
253	11	yeux	
254	10	accord	
255	10	apprendre	
256	10	avant	
257	10	capable	
258	10	conspire	
259	10	crains	
260	10	crois	
261	10	doute	
262	10	engage	
263	10	estime	
264	10	extrême	
265	10	fuir	
266	10	jour	
267	10	lâcheté	
268	10	naître	
269	10	nos	
270	10	oui	
271	10	palais	
272	10	quelle	
273	10	saurait	
274	10	souffrir	
275	10	trahir	
276	9	accable	
277	9	alors	
278	9	arrêt	
279	9	arrêter	
280	9	billet	
281	9	cent	
282	9	dessein	
283	9	dit	
284	9	douleur	
285	9	déjà	
286	9	faux	
287	9	heureux	
288	9	honorius	
289	9	illustre	
290	9	imposture	
291	9	injustice	
292	9	laisser	
293	9	marcellin	
294	9	mériter	
295	9	parler	
296	9	pouvez	
297	9	sais	
298	9	soit	
299	9	soupçons	
300	9	trouble	
301	9	vouloir	
302	9	voulu	
303	9	étonne	
304	8	accuse	
305	8	alaric	
306	8	bas	
307	8	bras	
308	8	ceux	
309	8	complice	
310	8	connais	
311	8	contraire	
312	8	courage	
313	8	dis	
314	8	disgrâce	
315	8	donner	
316	8	droit	
317	8	entreprendre	
318	8	faites	
319	8	force	
320	8	forcer	
321	8	haine	
322	8	honteux	
323	8	lieu	
324	8	loin	
325	8	mourir	
326	8	mérite	
327	8	noir	
328	8	outrage	
329	8	parle	
330	8	perfide	
331	8	peur	
332	8	redouter	
333	8	reste	
334	8	rigueur	
335	8	régner	
336	8	seule	
337	8	sous	
338	8	succès	
339	8	surpris	
340	8	venger	
341	8	viens	
342	8	vient	
343	8	éclater	
344	8	été	
345	7	ailleurs	
346	7	aimée	
347	7	amant	
348	7	attendre	
349	7	aurait	
350	7	avis	
351	7	cause	
352	7	coups	
353	7	dire	
354	7	douter	
355	7	dédaigner	
356	7	défense	
357	7	démentir	
358	7	désespoir	
359	7	eût	
360	7	feindre	
361	7	fois	
362	7	frère	
363	7	funeste	
364	7	haut	
365	7	immoler	
366	7	lucile	
367	7	lâches	
368	7	mal	
369	7	mille	
370	7	nom	
371	7	paraître	
372	7	pitié	
373	7	prends	
374	7	presse	
375	7	prétendre	
376	7	puisque	
377	7	sait	
378	7	sens	
379	7	sentiments	
380	7	soin	
381	7	soupçonner	
382	7	souvent	
383	7	sûr	
384	7	tâche	
385	7	vivre	
386	7	êtes	
387	6	agir	
388	6	anime	
389	6	apparence	
390	6	assure	
391	6	aucun	
392	6	aujourd	
393	6	aussitôt	
394	6	autres	
395	6	avez	
396	6	bassesse	
397	6	bout	
398	6	bruit	
399	6	celui	
400	6	chacun	
401	6	charmes	
402	6	cher	
403	6	complices	
404	6	crainte	
405	6	diadème	
406	6	défendre	
407	6	dérober	
408	6	désirs	
409	6	effroi	
410	6	ensemble	
411	6	entre	
412	6	flatter	
413	6	glorieux	
414	6	hui	
415	6	impératrice	
416	6	justice	
417	6	leurs	
418	6	menace	
419	6	monter	
420	6	mérité	
421	6	nature	
422	6	offrir	
423	6	personne	
424	6	peux	
425	6	plaire	
426	6	plein	
427	6	porte	
428	6	projet	
429	6	prévenir	
430	6	puisqu	
431	6	puisse	
432	6	raison	
433	6	revers	
434	6	soi	
435	6	soudain	
436	6	souffrez	
437	6	suspect	
438	6	taire	
439	6	tomber	
440	6	trahi	
441	6	trahit	
442	6	trouver	
443	6	téméraire	
444	6	vit	
445	6	voici	
446	5	abord	
447	5	accuser	
448	5	alarmes	
449	5	ambition	
450	5	appartement	
451	5	apprends	
452	5	aura	
453	5	aurez	
454	5	autorise	
455	5	avantage	
456	5	avouer	
457	5	beaucoup	
458	5	cabinet	
459	5	cependant	
460	5	cesse	
461	5	charme	
462	5	clartés	
463	5	complot	
464	5	confus	
465	5	conjurez	
466	5	consentir	
467	5	conspirer	
468	5	contraindre	
469	5	convainc	
470	5	courroux	
471	5	coûte	
472	5	croyez	
473	5	cède	
474	5	demande	
475	5	demeure	
476	5	donné	
477	5	droits	
478	5	dédains	
479	5	défaut	
480	5	détruit	
481	5	efforts	
482	5	emportement	
483	5	excuse	
484	5	faiblesse	
485	5	fasse	
486	5	faveur	
487	5	fit	
488	5	fruit	
489	5	heure	
490	5	hommage	
491	5	incapable	
492	5	jaloux	
493	5	jusques	
494	5	malheureux	
495	5	mis	
496	5	ni	
497	5	nier	
498	5	nommer	
499	5	objet	
500	5	oppose	
501	5	ouvre	
502	5	parlé	
503	5	parricide	
504	5	part	
505	5	passion	
506	5	perdu	
507	5	place	
508	5	placidie	
509	5	poignard	
510	5	prend	
511	5	prompte	
512	5	prêt	
513	5	puissance	
514	5	quitte	
515	5	refuse	
516	5	rendu	
517	5	réduit	
518	5	répondre	
519	5	serait	
520	5	sien	
521	5	soupirs	
522	5	soupçon	
523	5	suffit	
524	5	superbe	
525	5	supplice	
526	5	tel	
527	5	théodose	
528	5	tient	
529	5	trahison	
530	5	transports	
531	5	trépas	
532	5	vers	
533	5	vrai	
534	5	égal	
535	4	abandonne	
536	4	abuse	
537	4	admire	
538	4	affreux	
539	4	aide	
540	4	amitié	
541	4	appas	
542	4	arrogance	
543	4	artifice	
544	4	aspire	
545	4	assassin	
546	4	assurer	
547	4	atteinte	
548	4	attends	
549	4	attenter	
550	4	auraient	
551	4	aussi	
552	4	auteur	
553	4	avaient	
554	4	aveugle	
555	4	barbare	
556	4	bonté	
557	4	cacher	
558	4	chère	
559	4	colère	
560	4	confesse	
561	4	confond	
562	4	conseils	
563	4	couronner	
564	4	cris	
565	4	degré	
566	4	descendre	
567	4	destinée	
568	4	devait	
569	4	devant	
570	4	doive	
571	4	déguiser	
572	4	dépens	
573	4	dû	
574	4	envie	
575	4	espère	
576	4	espérance	
577	4	espérer	
578	4	eusse	
579	4	examiner	
580	4	excès	
581	4	expliquer	
582	4	faible	
583	4	favorable	
584	4	ferait	
585	4	fier	
586	4	fille	
587	4	forfaits	
588	4	forte	
589	4	garantir	
590	4	garde	
591	4	gênes	
592	4	hasarde	
593	4	injurieux	
594	4	intéresse	
595	4	jardin	
596	4	joie	
597	4	justifier	
598	4	lieux	
599	4	loi	
600	4	long	
601	4	malgré	
602	4	mettre	
603	4	moment	
604	4	murmure	
605	4	mutian	
606	4	noble	
607	4	noircir	
608	4	nomme	
609	4	nuit	
610	4	né	
611	4	obstacle	
612	4	obtenir	
613	4	offert	
614	4	ordonne	
615	4	oser	
616	4	ouïr	
617	4	passage	
618	4	percer	
619	4	perd	
620	4	perfidie	
621	4	permettez	
622	4	plaît	
623	4	pourquoi	
624	4	pourrais	
625	4	prenez	
626	4	presser	
627	4	preste	
628	4	preuves	
629	4	prince	
630	4	pris	
631	4	prison	
632	4	prompt	
633	4	prêter	
634	4	périr	
635	4	quelques	
636	4	quels	
637	4	rapport	
638	4	remède	
639	4	repentir	
640	4	rome	
641	4	satisfaire	
642	4	satisfait	
643	4	scrupule	
644	4	seconde	
645	4	secrets	
646	4	semble	
647	4	sensible	
648	4	sentiment	
649	4	songer	
650	4	souffre	
651	4	soupire	
652	4	soutienne	
653	4	suite	
654	4	supplices	
655	4	surprendre	
656	4	séduit	
657	4	sûreté	
658	4	tache	
659	4	titre	
660	4	tort	
661	4	tourments	
662	4	toutefois	
663	4	trembler	
664	4	triomphe	
665	4	triompher	
666	4	témoin	
667	4	témoins	
668	4	vengeance	
669	4	victime	
670	4	voulut	
671	4	éblouir	
672	4	éclairci	
673	4	élever	
674	4	étonner	
675	4	étouffer	
676	3	abus	
677	3	abuser	
678	3	accusé	
679	3	achève	
680	3	affront	
681	3	aigreur	
682	3	aimé	
683	3	aller	
684	3	animer	
685	3	appelle	
686	3	armes	
687	3	arrête	
688	3	assassiné	
689	3	attentats	
690	3	attente	
691	3	attirer	
692	3	auguste	
693	3	aurai	
694	3	aurais	
695	3	avertir	
696	3	avoue	
697	3	blesse	
698	3	blâmer	
699	3	bon	
700	3	bonheur	
701	3	bourreau	
702	3	brave	
703	3	braver	
704	3	cache	
705	3	calomnier	
706	3	certaine	
707	3	chagrin	
708	3	cherchant	
709	3	chose	
710	3	combat	
711	3	combattu	
712	3	conduire	
713	3	connaît	
714	3	conseil	
715	3	conspiré	
716	3	consulter	
717	3	contraint	
718	3	convaincre	
719	3	convaincu	
720	3	cour	
721	3	couronne	
722	3	cours	
723	3	craigne	
724	3	croit	
725	3	croître	
726	3	cruel	
727	3	dehors	
728	3	dernier	
729	3	derniers	
730	3	dessus	
731	3	différer	
732	3	donnant	
733	3	dédaigne	
734	3	dédire	
735	3	défend	
736	3	détourner	
737	3	effroyable	
738	3	employer	
739	3	enfance	
740	3	ennuis	
741	3	entendre	
742	3	entière	
743	3	envi	
744	3	es	
745	3	esprit	
746	3	eus	
747	3	exemple	
748	3	expier	
749	3	explique	
750	3	expose	
751	3	faille	
752	3	faits	
753	3	fatal	
754	3	ferme	
755	3	fidélité	
756	3	firent	
757	3	fière	
758	3	flanc	
759	3	flatte	
760	3	flatté	
761	3	font	
762	3	fort	
763	3	foudre	
764	3	fuite	
765	3	gardes	
766	3	gendre	
767	3	grandeur	
768	3	grands	
769	3	héros	
770	3	ignore	
771	3	ignorer	
772	3	immole	
773	3	impute	
774	3	incertaine	
775	3	indice	
776	3	indignité	
777	3	infidèle	
778	3	ingrate	
779	3	injuste	
780	3	inspire	
781	3	intérêts	
782	3	inutile	
783	3	jalouse	
784	3	jette	
785	3	jeté	
786	3	jouir	
787	3	juger	
788	3	laissant	
789	3	livre	
790	3	mains	
791	3	marque	
792	3	mets	
793	3	meurs	
794	3	mienne	
795	3	montre	
796	3	montrer	
797	3	moyens	
798	3	noire	
799	3	nécessaire	
800	3	oblige	
801	3	obscur	
802	3	obstine	
803	3	obéir	
804	3	occasion	
805	3	odieux	
806	3	opprime	
807	3	osant	
808	3	ouvrir	
809	3	paraît	
810	3	paru	
811	3	passe	
812	3	pense	
813	3	pieds	
814	3	plainte	
815	3	pleine	
816	3	pleinement	
817	3	pleurer	
818	3	plutôt	
819	3	pompejan	
820	3	porter	
821	3	porté	
822	3	pourrait	
823	3	pouvait	
824	3	prenant	
825	3	pressant	
826	3	pressé	
827	3	projets	
828	3	promis	
829	3	propose	
830	3	prouve	
831	3	prouver	
832	3	présumer	
833	3	puni	
834	3	pure	
835	3	purger	
836	3	put	
837	3	pût	
838	3	rare	
839	3	recevoir	
840	3	redoubler	
841	3	refuser	
842	3	regret	
843	3	rendez	
844	3	rends	
845	3	reprocher	
846	3	respects	
847	3	reçu	
848	3	rude	
849	3	rudes	
850	3	rufus	
851	3	sacrifice	
852	3	saisi	
853	3	sauver	
854	3	sein	
855	3	sera	
856	3	serais	
857	3	servir	
858	3	seulement	
859	3	seuls	
860	3	silence	
861	3	songe	
862	3	soupir	
863	3	soupçonne	
864	3	soutient	
865	3	souverain	
866	3	suivi	
867	3	superflus	
868	3	tandis	
869	3	tard	
870	3	tiens	
871	3	toucher	
872	3	tour	
873	3	tremble	
874	3	tristes	
875	3	vais	
876	3	vaste	
877	3	vaut	
878	3	verra	
879	3	victoire	
880	3	vif	
881	3	voila	
882	3	voix	
883	3	voudrait	
884	3	voulais	
885	3	voyant	
886	3	voyez	
887	3	éclate	
888	3	également	
889	3	éloigne	
890	3	élève	
891	2	abaisse	
892	2	abaissement	
893	2	abaisser	
894	2	abandonner	
895	2	abandonnez	
896	2	abandonnée	
897	2	abattu	
898	2	abominable	
899	2	absoudre	
900	2	accabler	
901	2	accident	
902	2	accorder	
903	2	accusant	
904	2	accusent	
905	2	acquiert	
906	2	adieu	
907	2	admirer	
908	2	admirez	
909	2	adouci	
910	2	adresse	
911	2	affermi	
912	2	aigrir	
913	2	aimable	
914	2	aimais	
915	2	allez	
916	2	altier	
917	2	ambitieux	
918	2	amis	
919	2	amorce	
920	2	amène	
921	2	aperçois	
922	2	applaudir	
923	2	apprenez	
924	2	appris	
925	2	approche	
926	2	apprête	
927	2	arrache	
928	2	arracher	
929	2	arrivé	
930	2	art	
931	2	artifices	
932	2	assassins	
933	2	asservir	
934	2	assurance	
935	2	attache	
936	2	attaque	
937	2	attend	
938	2	attendant	
939	2	auront	
940	2	autant	
941	2	avancé	
942	2	avenir	
943	2	avidité	
944	2	avouerai	
945	2	ayez	
946	2	balance	
947	2	balancer	
948	2	basse	
949	2	bienfaits	
950	2	bois	
951	2	bontés	
952	2	brillante	
953	2	calomnie	
954	2	caprice	
955	2	caractère	
956	2	causé	
957	2	celles	
958	2	certaines	
959	2	cesser	
960	2	chaleur	
961	2	chargé	
962	2	charmer	
963	2	charmé	
964	2	charmée	
965	2	chasser	
966	2	chemin	
967	2	chéris	
968	2	condamne	
969	2	condamner	
970	2	confesser	
971	2	confiance	
972	2	confier	
973	2	confirmer	
974	2	confondre	
975	2	connaissance	
976	2	connu	
977	2	couvrir	
978	2	coûter	
979	2	craignez	
980	2	craindrait	
981	2	crimes	
982	2	croirez	
983	2	croyant	
984	2	cruels	
985	2	crûs	
986	2	cédant	
987	2	céder	
988	2	daignez	
989	2	daigné	
990	2	dangereux	
991	2	dedans	
992	2	delà	
993	2	demain	
994	2	demandant	
995	2	demandez	
996	2	desseins	
997	2	devenir	
998	2	devez	
999	2	deviens	
1000	2	devrait	
1001	2	dieux	
1002	2	diffère	
1003	2	différence	
1004	2	dissipe	
1005	2	dissipé	
1006	2	donnez	
1007	2	douceur	
1008	2	douteux	
1009	2	due	
1010	2	dur	
1011	2	dures	
1012	2	dureté	
1013	2	dès	
1014	2	déclare	
1015	2	déclarez	
1016	2	découverte	
1017	2	découvrir	
1018	2	démenti	
1019	2	déplaisir	
1020	2	déplorable	
1021	2	dérobe	
1022	2	dût	
1023	2	effacer	
1024	2	emporte	
1025	2	emporté	
1026	2	empêche	
1027	2	encor	
1028	2	endurer	
1029	2	enfermé	
1030	2	ennemis	
1031	2	entier	
1032	2	entraîne	
1033	2	entretenir	
1034	2	entrez	
1035	2	escalier	
1036	2	esclave	
1037	2	estimer	
1038	2	eussent	
1039	2	examine	
1040	2	expire	
1041	2	exploits	
1042	2	exposer	
1043	2	facile	
1044	2	faisait	
1045	2	faisant	
1046	2	fallait	
1047	2	fameux	
1048	2	famille	
1049	2	faudra	
1050	2	fera	
1051	2	ferez	
1052	2	fermeté	
1053	2	fidèle	
1054	2	flammes	
1055	2	flattent	
1056	2	flavie	
1057	2	fléchir	
1058	2	forcé	
1059	2	formé	
1060	2	goth	
1061	2	grande	
1062	2	généreux	
1063	2	gêne	
1064	2	hais	
1065	2	hait	
1066	2	hardi	
1067	2	hasarder	
1068	2	hautement	
1069	2	honneur	
1070	2	hors	
1071	2	ignorant	
1072	2	imposteur	
1073	2	imposteurs	
1074	2	imprévu	
1075	2	indignes	
1076	2	infâme	
1077	2	ingratitude	
1078	2	injure	
1079	2	inquiet	
1080	2	inquiète	
1081	2	insigne	
1082	2	insolent	
1083	2	introduit	
1084	2	intéressant	
1085	2	intéressé	
1086	2	issue	
1087	2	jeter	
1088	2	jugez	
1089	2	juré	
1090	2	justes	
1091	2	laissez	
1092	2	laissé	
1093	2	liberté	
1094	2	loisir	
1095	2	lumières	
1096	2	lâchement	
1097	2	légitime	
1098	2	magnanime	
1099	2	maintenir	
1100	2	malheurs	
1101	2	mandant	
1102	2	maux	
1103	2	maîtresse	
1104	2	met	
1105	2	meure	
1106	2	moindre	
1107	2	moindres	
1108	2	moitié	
1109	2	morts	
1110	2	mots	
1111	2	mouvements	
1112	2	moyen	
1113	2	nobles	
1114	2	noeuds	
1115	2	nombre	
1116	2	nommé	
1117	2	notre	
1118	2	nuire	
1119	2	nécessité	
1120	2	nôtres	
1121	2	obliger	
1122	2	observer	
1123	2	oeil	
1124	2	offrait	
1125	2	opposer	
1126	2	ordres	
1127	2	osais	
1128	2	oubli	
1129	2	ouvert	
1130	2	ouverte	
1131	2	ouvrage	
1132	2	paix	
1133	2	pardonner	
1134	2	pardonnez	
1135	2	pareil	
1136	2	pareils	
1137	2	parlez	
1138	2	partager	
1139	2	parti	
1140	2	peines	
1141	2	peint	
1142	2	penchant	
1143	2	perdant	
1144	2	perds	
1145	2	persuader	
1146	2	peuple	
1147	2	plains	
1148	2	pleurs	
1149	2	poignardé	
1150	2	portez	
1151	2	pourra	
1152	2	pourrai	
1153	2	pourraient	
1154	2	pourtant	
1155	2	pourvoir	
1156	2	pousse	
1157	2	pousser	
1158	2	pouvaient	
1159	2	premier	
1160	2	priver	
1161	2	proche	
1162	2	prononcez	
1163	2	propice	
1164	2	près	
1165	2	prépare	
1166	2	prétends	
1167	2	pus	
1168	2	périsse	
1169	2	quelqu	
1170	2	quiconque	
1171	2	rares	
1172	2	remplissent	
1173	2	rencontre	
1174	2	rendra	
1175	2	repos	
1176	2	repousse	
1177	2	repousser	
1178	2	respect	
1179	2	respecte	
1180	2	reçois	
1181	2	reçue	
1182	2	rival	
1183	2	rompre	
1184	2	rougir	
1185	2	règle	
1186	2	règne	
1187	2	répond	
1188	2	réponde	
1189	2	résistance	
1190	2	résister	
1191	2	résoudre	
1192	2	résous	
1193	2	révolte	
1194	2	sachant	
1195	2	sauriez	
1196	2	savez	
1197	2	secrète	
1198	2	secrètement	
1199	2	semblez	
1200	2	sent	
1201	2	sentir	
1202	2	seront	
1203	2	servi	
1204	2	siens	
1205	2	soient	
1206	2	sois	
1207	2	sorti	
1208	2	souci	
1209	2	souffririez	
1210	2	souhaits	
1211	2	soupirer	
1212	2	source	
1213	2	soutenir	
1214	2	straton	
1215	2	suborné	
1216	2	suit	
1217	2	suivez	
1218	2	suivie	
1219	2	suivis	
1220	2	suivre	
1221	2	sujette	
1222	2	sut	
1223	2	séduire	
1224	2	séduisant	
1225	2	sûre	
1226	2	telle	
1227	2	tels	
1228	2	tendre	
1229	2	tendresses	
1230	2	tenté	
1231	2	tienne	
1232	2	tirer	
1233	2	tiré	
1234	2	tombeau	
1235	2	touche	
1236	2	trahis	
1237	2	traiter	
1238	2	traîtres	
1239	2	tremblant	
1240	2	trois	
1241	2	trompeur	
1242	2	trompée	
1243	2	trouvé	
1244	2	tumulte	
1245	2	vaincre	
1246	2	vaine	
1247	2	vanité	
1248	2	vaudra	
1249	2	venir	
1250	2	venu	
1251	2	verrait	
1252	2	verrons	
1253	2	vint	
1254	2	violence	
1255	2	violé	
1256	2	vis	
1257	2	visage	
1258	2	vivant	
1259	2	vive	
1260	2	vivez	
1261	2	voie	
1262	2	volontaire	
1263	2	vont	
1264	2	voudrais	
1265	2	voudras	
1266	2	voulez	
1267	2	vécu	
1268	2	véritable	
1269	2	vôtre	
1270	2	âge	
1271	2	âpre	
1272	2	échafaud	
1273	2	éclaircir	
1274	2	écoute	
1275	2	écoutez	
1276	2	éloignez	
1277	2	émouvoir	
1278	2	émue	
1279	2	épargne	
1280	2	épargner	
1281	2	épargné	
1282	2	époux	
1283	2	épreuve	
1284	2	étais	
1285	2	étalent	
1286	2	étant	
1287	2	éteindre	
1288	2	étonnant	
1289	2	étrange	
1290	2	éviter	
1291	1	abaissât	
1292	1	abandonnait	
1293	1	abandonné	
1294	1	abattons	
1295	1	abattre	
1296	1	absence	
1297	1	absolus	
1298	1	absout	
1299	1	abusé	
1300	1	abîmé	
1301	1	accablez	
1302	1	accepte	
1303	1	acceptât	
1304	1	accompagner	
1305	1	accordez	
1306	1	accordée	
1307	1	accourue	
1308	1	accusez	
1309	1	accusât	
1310	1	achever	
1311	1	achevez	
1312	1	achevé	
1313	1	acquérir	
1314	1	action	
1315	1	adoré	
1316	1	adoucie	
1317	1	adoucir	
1318	1	adoucit	
1319	1	adroit	
1320	1	adroitement	
1321	1	affecta	
1322	1	affecte	
1323	1	affectez	
1324	1	affermit	
1325	1	affligé	
1326	1	affranchir	
1327	1	affranchis	
1328	1	affreuses	
1329	1	afin	
1330	1	agis	
1331	1	agit	
1332	1	aida	
1333	1	aider	
1334	1	aidât	
1335	1	aigrira	
1336	1	aima	
1337	1	aimai	
1338	1	aimait	
1339	1	aimant	
1340	1	aisés	
1341	1	alarme	
1342	1	alarmé	
1343	1	alarmée	
1344	1	albin	
1345	1	alliance	
1346	1	allé	
1347	1	amener	
1348	1	amené	
1349	1	ami	
1350	1	amère	
1351	1	aperçoit	
1352	1	aposté	
1353	1	appeler	
1354	1	applaudie	
1355	1	apprend	
1356	1	apprendrai	
1357	1	apprendrez	
1358	1	approcher	
1359	1	apprêtez	
1360	1	arbitre	
1361	1	ardent	
1362	1	arma	
1363	1	armant	
1364	1	armé	
1365	1	armée	
1366	1	arrachent	
1367	1	arrêtant	
1368	1	arrêtez	
1369	1	arrêté	
1370	1	arrêtée	
1371	1	asile	
1372	1	aspirer	
1373	1	assassiner	
1374	1	assemble	
1375	1	assis	
1376	1	assouvie	
1377	1	assouvit	
1378	1	assujetti	
1379	1	assujettit	
1380	1	assurant	
1381	1	assurent	
1382	1	assureras	
1383	1	assurez	
1384	1	assuré	
1385	1	astre	
1386	1	attachent	
1387	1	attacher	
1388	1	attachez	
1389	1	attachée	
1390	1	attaquer	
1391	1	atteigne	
1392	1	atteindre	
1393	1	attendez	
1394	1	attendri	
1395	1	attenterait	
1396	1	aucune	
1397	1	auparavant	
1398	1	auprès	
1399	1	auriez	
1400	1	auteurs	
1401	1	autorisai	
1402	1	autorisât	
1403	1	autorité	
1404	1	autrui	
1405	1	avance	
1406	1	avancer	
1407	1	avertissant	
1408	1	aveuglement	
1409	1	avide	
1410	1	avides	
1411	1	aviez	
1412	1	avons	
1413	1	avéré	
1414	1	baigné	
1415	1	baissera	
1416	1	balançant	
1417	1	belle	
1418	1	besoin	
1419	1	bientôt	
1420	1	blessure	
1421	1	blâme	
1422	1	borner	
1423	1	bornes	
1424	1	bravant	
1425	1	bravent	
1426	1	braveront	
1427	1	bravez	
1428	1	brigue	
1429	1	brillant	
1430	1	briller	
1431	1	brise	
1432	1	brûlants	
1433	1	brûle	
1434	1	butte	
1435	1	cachaient	
1436	1	calmé	
1437	1	cercueil	
1438	1	certain	
1439	1	certitude	
1440	1	cessons	
1441	1	change	
1442	1	changeant	
1443	1	changer	
1444	1	charger	
1445	1	chaînes	
1446	1	chefs	
1447	1	cherchaient	
1448	1	cherchait	
1449	1	cherché	
1450	1	chers	
1451	1	chez	
1452	1	choisi	
1453	1	choisir	
1454	1	choisiras	
1455	1	choisirons	
1456	1	choisis	
1457	1	choisissez	
1458	1	chute	
1459	1	chéri	
1460	1	combats	
1461	1	combattait	
1462	1	combattre	
1463	1	combattue	
1464	1	commander	
1465	1	commence	
1466	1	commettez	
1467	1	commit	
1468	1	commun	
1469	1	compassion	
1470	1	complaisance	
1471	1	complots	
1472	1	comprendre	
1473	1	comprends	
1474	1	concerté	
1475	1	concevable	
1476	1	concevoir	
1477	1	condamnant	
1478	1	condamnée	
1479	1	conduit	
1480	1	conduite	
1481	1	confidence	
1482	1	confie	
1483	1	confondant	
1484	1	confondent	
1485	1	confronte	
1486	1	confronté	
1487	1	confuse	
1488	1	conjecture	
1489	1	connaissez	
1490	1	connus	
1491	1	conquête	
1492	1	conseiller	
1493	1	consens	
1494	1	consent	
1495	1	consente	
1496	1	consentirez	
1497	1	conservait	
1498	1	conserve	
1499	1	conserver	
1500	1	conservé	
1501	1	considérer	
1502	1	considériez	
1503	1	consoler	
1504	1	conspirait	
1505	1	conspirant	
1506	1	constance	
1507	1	consulte	
1508	1	content	
1509	1	contente	
1510	1	contenter	
1511	1	contents	
1512	1	contraigne	
1513	1	contrainte	
1514	1	convie	
1515	1	conçoit	
1516	1	conçu	
1517	1	conçut	
1518	1	corrigeons	
1519	1	corrompt	
1520	1	corrompu	
1521	1	corrompue	
1522	1	coulé	
1523	1	courant	
1524	1	coure	
1525	1	courez	
1526	1	couronnent	
1527	1	couronnée	
1528	1	court	
1529	1	coûtera	
1530	1	craignant	
1531	1	craindrai	
1532	1	craindrais	
1533	1	creusé	
1534	1	croie	
1535	1	croirai	
1536	1	croyais	
1537	1	croyait	
1538	1	crue	
1539	1	crut	
1540	1	crédule	
1541	1	crûssiez	
1542	1	crût	
1543	1	cédé	
1544	1	césars	
1545	1	daigna	
1546	1	daigne	
1547	1	daigner	
1548	1	daigneriez	
1549	1	davantage	
1550	1	demandée	
1551	1	demeurant	
1552	1	demeuré	
1553	1	depuis	
1554	1	descendez	
1555	1	descends	
1556	1	deviner	
1557	1	devoirs	
1558	1	devrai	
1559	1	devraient	
1560	1	devrais	
1561	1	différent	
1562	1	dignes	
1563	1	dignité	
1564	1	dirai	
1565	1	dirais	
1566	1	discerner	
1567	1	discours	
1568	1	discret	
1569	1	dispense	
1570	1	dispenser	
1571	1	dispose	
1572	1	dites	
1573	1	divers	
1574	1	dominant	
1575	1	don	
1576	1	donnerais	
1577	1	douce	
1578	1	douces	
1579	1	douceurs	
1580	1	douleurs	
1581	1	doutes	
1582	1	doutez	
1583	1	doutons	
1584	1	durs	
1585	1	dussai	
1586	1	dusse	
1587	1	décevant	
1588	1	déclarer	
1589	1	découvre	
1590	1	dédaignant	
1591	1	dédaignera	
1592	1	dédaignes	
1593	1	dédaigné	
1594	1	défaire	
1595	1	défaite	
1596	1	défera	
1597	1	déferra	
1598	1	défiance	
1599	1	défiant	
1600	1	défié	
1601	1	déférence	
1602	1	déguisant	
1603	1	déguisements	
1604	1	déguisera	
1605	1	délivre	
1606	1	démens	
1607	1	dément	
1608	1	dépendre	
1609	1	déplaire	
1610	1	dépose	
1611	1	dérobait	
1612	1	dérobez	
1613	1	dérobons	
1614	1	dérobé	
1615	1	désabusent	
1616	1	désaveu	
1617	1	désavouant	
1618	1	désavoue	
1619	1	désir	
1620	1	désobéir	
1621	1	désordre	
1622	1	détestable	
1623	1	détestant	
1624	1	détour	
1625	1	détournant	
1626	1	détruire	
1627	1	détruite	
1628	1	développer	
1629	1	déçu	
1630	1	efface	
1631	1	effacé	
1632	1	effaçons	
1633	1	effets	
1634	1	effrayée	
1635	1	embarras	
1636	1	embarrasse	
1637	1	embrasse	
1638	1	embrassements	
1639	1	embrassent	
1640	1	embrasser	
1641	1	emploie	
1642	1	employais	
1643	1	emportait	
1644	1	empressement	
1645	1	empêcher	
1646	1	empêchez	
1647	1	empêché	
1648	1	enclin	
1649	1	endurci	
1650	1	endurcie	
1651	1	endurcir	
1652	1	endurcissement	
1653	1	enfants	
1654	1	enfer	
1655	1	enflamme	
1656	1	enflammer	
1657	1	enfle	
1658	1	enfoncé	
1659	1	ennemi	
1660	1	ennui	
1661	1	enraciné	
1662	1	entend	
1663	1	entends	
1664	1	entr	
1665	1	entrait	
1666	1	entreprenait	
1667	1	entreprendrai	
1668	1	entrepris	
1669	1	entrer	
1670	1	entretiens	
1671	1	envier	
1672	1	environnaient	
1673	1	envoie	
1674	1	envoyer	
1675	1	escorte	
1676	1	esprits	
1677	1	espèrent	
1678	1	espérais	
1679	1	espérez	
1680	1	essayez	
1681	1	estimable	
1682	1	estiment	
1683	1	eu	
1684	1	eurent	
1685	1	eussiez	
1686	1	eust	
1687	1	evodius	
1688	1	exacte	
1689	1	examinera	
1690	1	examinerai	
1691	1	excitais	
1692	1	excite	
1693	1	excuser	
1694	1	excusez	
1695	1	exige	
1696	1	expierait	
1697	1	expliquera	
1698	1	expliqué	
1699	1	extrémités	
1700	1	exécute	
1701	1	exécuter	
1702	1	faiblesses	
1703	1	faisais	
1704	1	fatale	
1705	1	fatalité	
1706	1	faudrait	
1707	1	fausse	
1708	1	faute	
1709	1	favorables	
1710	1	favorise	
1711	1	favorisent	
1712	1	façon	
1713	1	feignant	
1714	1	feignit	
1715	1	feinte	
1716	1	femme	
1717	1	fer	
1718	1	feront	
1719	1	fers	
1720	1	feux	
1721	1	fidèles	
1722	1	fini	
1723	1	fis	
1724	1	fisse	
1725	1	flatteurs	
1726	1	flexible	
1727	1	fléchisse	
1728	1	forcerons	
1729	1	forces	
1730	1	forcez	
1731	1	formaient	
1732	1	formant	
1733	1	formée	
1734	1	fortifier	
1735	1	franchise	
1736	1	frappe	
1737	1	frappé	
1738	1	frappée	
1739	1	fuis	
1740	1	fuit	
1741	1	funestes	
1742	1	fus	
1743	1	fuyaient	
1744	1	fuyant	
1745	1	fâcher	
1746	1	fît	
1747	1	gagner	
1748	1	gagnez	
1749	1	garant	
1750	1	garants	
1751	1	garder	
1752	1	gardez	
1753	1	gauchit	
1754	1	genre	
1755	1	gouffre	
1756	1	gouvernement	
1757	1	goûtai	
1758	1	goûtant	
1759	1	goûte	
1760	1	grandes	
1761	1	grandeurs	
1762	1	gratian	
1763	1	grossières	
1764	1	guère	
1765	1	généreuse	
1766	1	générosité	
1767	1	ha	
1768	1	hardis	
1769	1	hasards	
1770	1	hauts	
1771	1	haïr	
1772	1	haïssez	
1773	1	heur	
1774	1	honneurs	
1775	1	honteuse	
1776	1	hyménée	
1777	1	hâte	
1778	1	hâter	
1779	1	hésite	
1780	1	idole	
1781	1	ignominie	
1782	1	ignoré	
1783	1	imaginez	
1784	1	immobile	
1785	1	immolai	
1786	1	immolé	
1787	1	imparfait	
1788	1	imparfaite	
1789	1	imparfaites	
1790	1	impatience	
1791	1	impiété	
1792	1	important	
1793	1	importe	
1794	1	imposant	
1795	1	impose	
1796	1	imprime	
1797	1	imprudence	
1798	1	imprudent	
1799	1	impuissance	
1800	1	imputer	
1801	1	imputé	
1802	1	incertain	
1803	1	incertitude	
1804	1	indifférente	
1805	1	indignez	
1806	1	indispensable	
1807	1	infaillible	
1808	1	inflexible	
1809	1	informe	
1810	1	infortuné	
1811	1	ingrats	
1812	1	injures	
1813	1	injustement	
1814	1	injustes	
1815	1	insolemment	
1816	1	insolence	
1817	1	insolente	
1818	1	inspirant	
1819	1	instant	
1820	1	instinct	
1821	1	instruire	
1822	1	instruit	
1823	1	instruits	
1824	1	insupportable	
1825	1	interdit	
1826	1	interdite	
1827	1	intrigues	
1828	1	intéresser	
1829	1	inventez	
1830	1	invincible	
1831	1	inébranlable	
1832	1	inégal	
1833	1	inégale	
1834	1	inévitable	
1835	1	irait	
1836	1	italie	
1837	1	jeune	
1838	1	jeunesse	
1839	1	joindre	
1840	1	joint	
1841	1	juge	
1842	1	jugé	
1843	1	jure	
1844	1	jurer	
1845	1	jusque	
1846	1	justifié	
1847	1	laissiez	
1848	1	laissât	
1849	1	languissant	
1850	1	large	
1851	1	larmes	
1852	1	lassé	
1853	1	laverait	
1854	1	lent	
1855	1	libérateur	
1856	1	lie	
1857	1	lis	
1858	1	lit	
1859	1	livrait	
1860	1	livré	
1861	1	lois	
1862	1	longs	
1863	1	longtemps	
1864	1	louera	
1865	1	lucilian	
1866	1	lumière	
1867	1	maintenant	
1868	1	maligne	
1869	1	mandé	
1870	1	manie	
1871	1	manquait	
1872	1	manquant	
1873	1	manque	
1874	1	manquent	
1875	1	marquer	
1876	1	marques	
1877	1	martyre	
1878	1	maxence	
1879	1	maîtresses	
1880	1	menacé	
1881	1	mettrais	
1882	1	miens	
1883	1	milieu	
1884	1	misérable	
1885	1	monde	
1886	1	montrant	
1887	1	montrent	
1888	1	montrerait	
1889	1	montrez	
1890	1	monté	
1891	1	motif	
1892	1	motifs	
1893	1	mourais	
1894	1	mourra	
1895	1	mourrai	
1896	1	mouvement	
1897	1	muet	
1898	1	mystère	
1899	1	mystérieux	
1900	1	méchant	
1901	1	mémoire	
1902	1	méprisez	
1903	1	méritaient	
1904	1	méritais	
1905	1	mêlent	
1906	1	mêlera	
1907	1	naissent	
1908	1	naquit	
1909	1	nierais	
1910	1	nierait	
1911	1	noirci	
1912	1	nommez	
1913	1	nourrir	
1914	1	nouveau	
1915	1	nouveaux	
1916	1	nouvel	
1917	1	nouvelle	
1918	1	nouvelles	
1919	1	née	
1920	1	négliger	
1921	1	négligé	
1922	1	nôtre	
1923	1	observe	
1924	1	obstines	
1925	1	obstiné	
1926	1	obstinée	
1927	1	obtenu	
1928	1	obtiendrait	
1929	1	obtienne	
1930	1	obtiens	
1931	1	obtins	
1932	1	obéisse	
1933	1	occupait	
1934	1	occupe	
1935	1	offensait	
1936	1	offerts	
1937	1	officieux	
1938	1	offrent	
1939	1	ombre	
1940	1	opprimée	
1941	1	opprobre	
1942	1	orage	
1943	1	ordinaire	
1944	1	ordonnant	
1945	1	ordonner	
1946	1	ordonné	
1947	1	osa	
1948	1	osait	
1949	1	osent	
1950	1	oserai	
1951	1	oseriez	
1952	1	osez	
1953	1	oublier	
1954	1	oublierez	
1955	1	outre	
1956	1	ouïs	
1957	1	parais	
1958	1	paraisse	
1959	1	parce	
1960	1	parlant	
1961	1	parlent	
1962	1	parles	
1963	1	parlons	
1964	1	parmi	
1965	1	parricides	
1966	1	partage	
1967	1	partageant	
1968	1	partagé	
1969	1	passais	
1970	1	passer	
1971	1	passions	
1972	1	passé	
1973	1	payée	
1974	1	peignait	
1975	1	peins	
1976	1	pensant	
1977	1	penser	
1978	1	pensé	
1979	1	percent	
1980	1	perdez	
1981	1	permet	
1982	1	permettre	
1983	1	permis	
1984	1	permit	
1985	1	persistez	
1986	1	peuvent	
1987	1	piège	
1988	1	placer	
1989	1	plaignaient	
1990	1	plaignant	
1991	1	plaindrais	
1992	1	pleure	
1993	1	plonge	
1994	1	plupart	
1995	1	poignarde	
1996	1	poignarder	
1997	1	politique	
1998	1	portais	
1999	1	possède	
2000	1	pourriez	
2001	1	pourrons	
2002	1	pourront	
2003	1	poursuit	
2004	1	poursuite	
2005	1	poursuivre	
2006	1	pourvu	
2007	1	poussez	
2008	1	poussons	
2009	1	pouvais	
2010	1	pouvant	
2011	1	pouviez	
2012	1	pratiques	
2013	1	premiers	
2014	1	prendra	
2015	1	prendrait	
2016	1	presque	
2017	1	prisonnière	
2018	1	prive	
2019	1	profonds	
2020	1	promesse	
2021	1	promptement	
2022	1	prononçai	
2023	1	propre	
2024	1	protéger	
2025	1	prudence	
2026	1	prudent	
2027	1	précaution	
2028	1	précieux	
2029	1	précipitant	
2030	1	précipité	
2031	1	préoccupée	
2032	1	présence	
2033	1	présentée	
2034	1	préserver	
2035	1	préside	
2036	1	présumé	
2037	1	prévaloir	
2038	1	préviendra	
2039	1	préviendrons	
2040	1	préviens	
2041	1	prévoir	
2042	1	prévois	
2043	1	prévu	
2044	1	prêté	
2045	1	publique	
2046	1	publié	
2047	1	puissant	
2048	1	puissent	
2049	1	puisses	
2050	1	punie	
2051	1	puniriez	
2052	1	punissant	
2053	1	pur	
2054	1	pureté	
2055	1	purgerait	
2056	1	pénétrer	
2057	1	périrais	
2058	1	qualité	
2059	1	quelles	
2060	1	quitter	
2061	1	quittez	
2062	1	quitté	
2063	1	quoiqu	
2064	1	race	
2065	1	racheter	
2066	1	raisons	
2067	1	ramasser	
2068	1	ramène	
2069	1	ravale	
2070	1	ravi	
2071	1	ravir	
2072	1	rebelle	
2073	1	recevrez	
2074	1	reconnaissance	
2075	1	recourir	
2076	1	recueillir	
2077	1	redouble	
2078	1	redoutant	
2079	1	refroidie	
2080	1	refroidir	
2081	1	refusant	
2082	1	refusez	
2083	1	refusé	
2084	1	regarde	
2085	1	regards	
2086	1	regrets	
2087	1	rehausser	
2088	1	reine	
2089	1	rejeta	
2090	1	rejeté	
2091	1	relevé	
2092	1	relâchant	
2093	1	relâche	
2094	1	remarque	
2095	1	rempli	
2096	1	remplir	
2097	1	remplis	
2098	1	remuements	
2099	1	rendent	
2100	1	rendrait	
2101	1	rendront	
2102	1	renonce	
2103	1	renoncer	
2104	1	renonçant	
2105	1	renverser	
2106	1	repent	
2107	1	repoussent	
2108	1	repris	
2109	1	reprochez	
2110	1	respecta	
2111	1	respecter	
2112	1	ressentiment	
2113	1	rester	
2114	1	restez	
2115	1	retient	
2116	1	retire	
2117	1	retour	
2118	1	retourne	
2119	1	retraite	
2120	1	revoir	
2121	1	rois	
2122	1	rompant	
2123	1	rougis	
2124	1	rougit	
2125	1	rufin	
2126	1	ruse	
2127	1	règnent	
2128	1	rébellion	
2129	1	récompense	
2130	1	récompenser	
2131	1	récuse	
2132	1	réduite	
2133	1	régla	
2134	1	réglait	
2135	1	régler	
2136	1	répandu	
2137	1	réparer	
2138	1	répondrai	
2139	1	répondu	
2140	1	réserve	
2141	1	réserver	
2142	1	réservons	
2143	1	résolu	
2144	1	résolue	
2145	1	résolve	
2146	1	résout	
2147	1	réussir	
2148	1	révoltés	
2149	1	révélé	
2150	1	sache	
2151	1	sachez	
2152	1	sacrez	
2153	1	sacrifiez	
2154	1	sages	
2155	1	saoulé	
2156	1	satisfaite	
2157	1	saura	
2158	1	saurai	
2159	1	saurais	
2160	1	saurez	
2161	1	sauvé	
2162	1	savaient	
2163	1	savent	
2164	1	sceptre	
2165	1	second	
2166	1	secourir	
2167	1	secouru	
2168	1	secrètes	
2169	1	selon	
2170	1	sembla	
2171	1	semblais	
2172	1	semblait	
2173	1	sembles	
2174	1	semblé	
2175	1	semence	
2176	1	sentier	
2177	1	sentis	
2178	1	serai	
2179	1	servent	
2180	1	service	
2181	1	services	
2182	1	servirait	
2183	1	sienne	
2184	1	signaler	
2185	1	signe	
2186	1	simple	
2187	1	sitôt	
2188	1	soif	
2189	1	solide	
2190	1	solliciter	
2191	1	songeais	
2192	1	songez	
2193	1	sorte	
2194	1	sortir	
2195	1	souffert	
2196	1	soufferte	
2197	1	souffrant	
2198	1	souffrirai	
2199	1	souhait	
2200	1	souhaite	
2201	1	souilla	
2202	1	souille	
2203	1	souillé	
2204	1	soulager	
2205	1	souscrire	
2206	1	soutenait	
2207	1	soutenez	
2208	1	soutien	
2209	1	soutint	
2210	1	souveraine	
2211	1	spectacle	
2212	1	splendeur	
2213	1	stupide	
2214	1	stupidité	
2215	1	sue	
2216	1	suivant	
2217	1	suivra	
2218	1	supposé	
2219	1	suprême	
2220	1	surprenant	
2221	1	surprenants	
2222	1	surtout	
2223	1	survive	
2224	1	survivre	
2225	1	sus	
2226	1	suspendant	
2227	1	suspendu	
2228	1	séduite	
2229	1	séduits	
2230	1	sépare	
2231	1	séparément	
2232	1	sévère	
2233	1	sûrs	
2234	1	tantôt	
2235	1	teint	
2236	1	telles	
2237	1	terence	
2238	1	termine	
2239	1	ternir	
2240	1	teste	
2241	1	theodot	
2242	1	tiennent	
2243	1	tire	
2244	1	tombe	
2245	1	touché	
2246	1	tourne	
2247	1	trahirais	
2248	1	trahisons	
2249	1	trahissait	
2250	1	trahissant	
2251	1	trahisse	
2252	1	traitant	
2253	1	traitement	
2254	1	traits	
2255	1	traité	
2256	1	trame	
2257	1	tranchent	
2258	1	transporte	
2259	1	travaux	
2260	1	traîne	
2261	1	traîner	
2262	1	tremblante	
2263	1	tremblerait	
2264	1	tremblerez	
2265	1	tremblez	
2266	1	tremblé	
2267	1	tremper	
2268	1	triomphez	
2269	1	trompe	
2270	1	tromper	
2271	1	trompé	
2272	1	troupe	
2273	1	trouva	
2274	1	trouvent	
2275	1	trouveront	
2276	1	trêve	
2277	1	tue	
2278	1	tyran	
2279	1	tâchait	
2280	1	tâché	
2281	1	témoigner	
2282	1	térence	
2283	1	tôt	
2284	1	uni	
2285	1	union	
2286	1	univers	
2287	1	uns	
2288	1	usage	
2289	1	vaincrons	
2290	1	vaines	
2291	1	vains	
2292	1	valere	
2293	1	vas	
2294	1	veille	
2295	1	venais	
2296	1	vengeant	
2297	1	verrai	
2298	1	verrions	
2299	1	verser	
2300	1	versé	
2301	1	viendra	
2302	1	viendrons	
2303	1	viennent	
2304	1	vigueur	
2305	1	ville	
2306	1	vingt	
2307	1	viole	
2308	1	vives	
2309	1	vivrait	
2310	1	vole	
2311	1	volonté	
2312	1	voudra	
2313	1	voudraient	
2314	1	voulait	
2315	1	voulant	
2316	1	voulût	
2317	1	voyaient	
2318	1	voyais	
2319	1	voyait	
2320	1	vrais	
2321	1	vue	
2322	1	vérité	
2323	1	vérités	
2324	1	vît	
2325	1	zélé	
2326	1	zélés	
2327	1	âmes	
2328	1	éblouis	
2329	1	éblouissait	
2330	1	éblouisse	
2331	1	éblouissent	
2332	1	ébranler	
2333	1	ébranlé	
2334	1	écart	
2335	1	échappé	
2336	1	échauffe	
2337	1	éclairer	
2338	1	éclairé	
2339	1	éclatant	
2340	1	éclatante	
2341	1	écouter	
2342	1	écouté	
2343	1	égaux	
2344	1	élevait	
2345	1	élevant	
2346	1	élevé	
2347	1	éloignant	
2348	1	éloigner	
2349	1	émeut	
2350	1	épargnez	
2351	1	épargniez	
2352	1	épargnons	
2353	1	épars	
2354	1	épousant	
2355	1	épouvanté	
2356	1	épris	
2357	1	éprouva	
2358	1	épuise	
2359	1	épée	
2360	1	étendit	
2361	1	éternels	
2362	1	étincelants	
2363	1	étions	
2364	1	étonnent	
2365	1	étonnez	
2366	1	étonné	
2367	1	étouffée	
2368	1	étourdis	
2369	1	étroit	
2370	1	étudie	
2371	1	évader	
2372	1	ôte	
2373	1	ôteront	
2374	1	ôtez	
